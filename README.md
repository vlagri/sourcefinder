# README #

**This program aims to identify the source of whole or partial sequences as chromosome, plasmid and phage using machine learning.**

The program is composed of scripts for data preparation, machine learning and assessment of machine learning results. There is also an automated pipeline available to run 
all the scripts with one command. 

This program is utilized for identifying source of over 20000 sequences as described in Aytan-Aktug et al., 2022. The results are also available in the results 
directory. 

## Getting started 

    git clone https://bitbucket.org/vlagri/sourcefinder.git

## SourceFinder 

The first pipeline called SourceFinder_single.py takes a single input file and sub-samples a random 5000 nucleotide region for a given number of times. This sampled region is sub-sequenced into 5-mers. The saved fragment model was utilized for predicting the origin of the input data. 
Random seed should be provided by the user for the reproducibility of the results. The final prediction is made based on the average class probabilities over fragments and the five models. 

    usage: SourceFinder_single.py [-h] [-s SEED] [-p PATH] [-n SAMPLE_N] [input_file]

ORIGIN project: data preprocessing & ensemble model prediction pipeline.

REQUIRES: a sequence file (.fna/.fasta)

positional arguments:

  input_file:            Path to the assemblies to be analysed

optional arguments:

  -h, --help            show this help message and exit
  
  -s SEED, --seed SEED  Random seed
  
  -p PATH, --path PATH  Path to the models
  
  -n SAMPLING_N, --sampling_n SAMPLING_N
                        Sampling number
                        
The second pipeline called SourceFinder_directory.py takes a directory which might include single or multiple sequences and sub-sample a random 5000 nucleotide region from
each sequence for a given number of times. This sampled region is sub-sequenced into 5-mers. The saved fragment model was utilized for predicting the origin of the input data. 
Random seed should be provided by the user for the reproducibility of the results. The final predictions made is based on the average class probabilities over the fragments and the five models. 

    usage: SourceFinder_directory.py [-h] [-s SEED] [-p PATH] [-n SAMPLE_N] [-o OUTPUT] [input_dir]

ORIGIN project: data preprocessing & ensemble model prediction pipeline.

REQUIRES: a sequence file (.fna/.fasta)

positional arguments:

  input_dir:             Path to the directory with assemblies to be
                        analyzed

optional arguments:

  -h, --help            show this help message and exit
  
  -s SEED, --seed SEED  Random seed
  
  -p PATH, --path PATH  Path to the models
  
  -n SAMPLING_N, --sampling_n SAMPLING_N
                        Sampling number
                        
  -o OUTPUT, --output OUTPUT  Output directory                        
                        
## Download trained models

The fragment models that trained with 5-mers of 5000 nucleotide fragments from ten rounds of sampling can be downloaded with the following command:

    wget ftp://ftp.cbs.dtu.dk/public/CGE/databases/OriginFinder/*

## Example usage

    python3 SourceFinder_single.py -s 42 -p /path/to/saved/models -n 5 /path/to/your_contig.fasta
    
    python3 SourceFinder_directory.py -s 42 -p /path/to/saved/models -n 5 -o /path/to/your/output/dir /path/to/your/input/dir

## Training from scratch

## Useful information 

 * The model training can be done from scratch by following the data preprocessing and machine learning steps below. If the input datasets (fasta files) are already available, step-0 of the data preprocessing should be skipped. All the 
scripts are for the data preparation steps are in the data_preprocessing folder. 

  * After preparing the input matrices by following the data preprocessing steps, the models can be trained using the scripts in the randomforest_models folder. The script CV_plotting_functions.py is not a script for training, it includes the
essential functions for training. 

  * The scripts for testing the new data with the already trained models, the pipelines should be used. The pipelines are available in the SourceFinder folder. The trained 
  machine learning models required for the pipelines are available in the ftp side (ftp://ftp.cbs.dtu.dk/public/CGE/databases/OriginFinder/).  
  
  * For more details, please read each section carefully.

## Data preprocessing

The data preprocessing scripts take chromosome, plasmid and phage assemblies and sub-sample into k-mers. Metrices are generated from the k-mer frequencies per class.
The metrices are merged after normalized into a single matrix. This matrix is used for training, testing and validating the machine learning models which is explained in the next section.  

The summary of the workflow:

![picture](/results/dig_data_prep.PNG){width=30 height=30}

### STEP 0: util_download_data.py

This Python script downloads genomic data from the PATRIC's ftp side in parallel.
The URL format is as follows: "ftp://ftp.patricbrc.org/genomes/{}/{}.fna".format(id_, id_)
The script accepts a file containing a list of genome IDs to be retrieved (example: '1010615.9')
    
  positional arguments:
      
  ids_file:        List of IDs to retrieve from the PATRIC's database 
  
  to_dir:        Path to the output directory
    
### STEP 1: util_fragments_extraction.py
 
This Python script generates fragments from the whole sequences for a given size and a given number of times to sample. This step is only necessary if the models will be trained with 
the fragments. 

It requires the path to the input file, path to the output file, fragment size, and number of sampling. The input files will be the fasta files of chromosomes,
plasmids and phages (step-0). This program takes one file at a time. Therefore, for the multiple files, it should be run inside in a loop. 

Example loop: 

    #!/bin/bash
    
    ls /path/to/kmc.kms > files.txt
    
    for i in $(cat files.txt)
    
    do
    
    python3 util_fragments_extraction.py -f /path/to/dir/$i -o /path/to/outdir -s 5000 -n 2
    
    sleep 1s
    
    done

Program usage:

    usage: util_fragments_extraction.py [-h] [-f FILE_PATH] [-o OUTPUT_DIR] [-s SIZE] [-n NUMBER]

FUN: truncate sequence files from random sequence start to specified length.
The function returns a fasta record, with original extension, of truncated
length. If raw sequence is shorter, the function will return everything in the
entry.

optional arguments:

  -h, --help:            show this help message and exit
  
  -f FILE_PATH, --file_path FILE_PATH: Path to the input file
  
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR: Path to the output directory
  
  -s SIZE, --size SIZE: Specify the sequence size
  
  -n NUMBER, --number NUMBER: Number of times to sample

### STEP 2: util_kmc_single.py

This Python script subsamples the input files into k-mers for a given k. 

It requires the path to the input file, path to the output directory and k-mer size. The input files would be either the whole fasta (step-0) or truncated fasta files that generated 
in the previous step (step-1). This program takes one file at a time. Therefore, for the multiple files, it should be run inside in a loop. 

    usage: util_kmc_single.py [-h] [file_path] [output_dir] [kmer_size]

KMC: sub-sequence a sequence file into k-mers using a specified size

positional arguments:

  file_path:        Path to the input file
  
  output_dir:       Path to the output directory
  
  kmer_size:        Specify kmer_size

optional arguments:

  -h, --help  show this help message and exit

### STEP 3: util_combine_kmc_counts.py

This Python script takes all the k-mer frequency files produced at step-2 and joins them into a matrix. The matrix rows correspond to sequences, the columns are k-mers and the entries are the k-mer frequencies. 

It requires a file with all possible k-mers (possible k-mers will be used in the matrix), path to the list with k-mer files (e.g. KMC output files) and the output file name for the matrix. 

Example of the file including all possible k-mers is available here: https://bitbucket.org/vlagri/oriclass/src/master/SourceFinder/models/frg_x10/

All KMC files can be collected into a single file using this example comment: 

    find /path/to/kmc/files > all_kmc.txt

    usage: util_combine_kmc_counts.py [-h] [-a ALL_COMB] [-i INPUT_LIST] [-o OUTPUT_FILE]

Matrix preparation using pre-allocated memory space and dictionary iteration

optional arguments:

  -h, --help:            show this help message and exit
  
  -a ALL_COMB, --all_comb ALL_COMB: File with all possible kmers
  
  -i INPUT_LIST, --input_list INPUT_LIST: Path to the list with KMC file paths
  
  -o OUTPUT_FILE, --output_file OUTPUT_FILE: Full path name of the output matrix file

### STEP 4: util_normalize_kmc_count_mtx.py

This Python script normalizes the k-mer frequencies produced at step-3 by dividing the specific k-mer frequency by its total number of k-mers. 
Additionally, the script adds a label column for supervised ML.

It requires the path to the matrix file generated at step-3, path to the output matrix, a multiplication scaler to avoid too small numbers, and the sequence label.

    usage: util_normalize_kmc_count_mtx.py [-h] [-m MTX_FILE] [-o OUTPUT_FILE] [-s SCALER] [-g ORIGIN]

Matrix normalization and class assignment

optional arguments:

  -h, --help:            show this help message and exit
  
  -m MTX_FILE, --mtx_file MTX_FILE: Path to the input matrix file
  
  -o OUTPUT_FILE, --output_file OUTPUT_FILE: Full path name of the output matrix file
  
  -s SCALER, --scaler SCALER: A multiplication scaler: default=1000000
  
  -g ORIGIN, --origin ORIGIN: Level of the counts origin <int>: 1:Chromosome, 0:Plasmid, -1:Phage

### STEP 5: util_mtx_to_df.py

The previous step requires stripping of non nunmerical data for more efficient processing (relevant to large datasets), hence for ease of use downstream, 
we need to append the "metadata" such as sample and column (k-mer) names.
This Python scripts add identifiers such as k-mers and genome id to the matrix produced at step-4.

It requires the path to the input matrix, path to the output matrix, file with all possible k-mers (https://bitbucket.org/vlagri/oriclass/src/master/SourceFinder/models/frg_x10/) and path to the list with all included k-mer files (to be used as sequence IDs).

    usage: util_mtx_to_df.py [-h] [mtx_file] [output_file] [kmer_list] [file_list]

"Annotate" matrix with kmer columns and isolate names

positional arguments:

  mtx_file:     Path to the input matrix file
  
  output_file:  Path to the output file
  
  kmer_list:    Path to the all kmers list
  
  file_list:    Path to the all files list

optional arguments:

  -h, --help   show this help message and exit

### STEP 6: util_combine_datasets_shuffle_and_split.py

This Python script combines the individual datasets (per chromosome, plasmid and phage) produced at step-5 into a single matrix, shuffles the entries and splits the file into training and holdout datasets.

It requires a file which contains a list of datasets, name for the output matrix, path for the output matrix, random state for the reproducibility, and hold-out size.

The file containing the list of datasets will be in this format:

/Path/to/the/chromosome/matrix

/Path/to/the/plasmid/matrix

/Path/to/the/phage/matrix

Where each matrix was produced at step-5. 


    usage: util_combine_datasets_shuffle_and_split.py [-h] [-f FILE_LIST] [-o OUTPUT_FILE] [-d OUTPUT_DIR] [-r RANDOM_STATE] [-hs HOLDOUT_SIZE]

Combining datasets

optional arguments:

  -h, --help:            show this help message and exit
  
  -f FILE_LIST, --file_list FILE_LIST: List with the datasets' paths
  
  -o OUTPUT_FILE, --output_file OUTPUT_FILE: Full path name of the output matrix file
  
  -d OUTPUT_DIR, --output_dir OUTPUT_DIR: Path to the output directory
  
  -r RANDOM_STATE, --random_state RANDOM_STATE: Random state value
  
  -hs HOLDOUT_SIZE, --holdout_size HOLDOUT_SIZE: Holdout size (float)


## Machine learning

This program allows to generate three different random forest models. The first model is trained and tested with the whole chromosome, plasmid and phage sequences.
The second model is trained with the whole sequences but tested with the truncated 5000 nucleotide sequences. And the third model is trained and tested with the
truncated sequences but they are sampled from the sequences multiple times. 

### RF_whole_sequences_eval_CV.py

This Python script takes the matrix containing chromosome, plasmid and phage k-mer frequencies from step-6 and runs random forest model using the 5-fold cross-validation method. The model is also tested with the whole sequences.

It requires the path to the training dataset, path to the hold-out dataset, and path to the output directory. The output directory will contain following files:
ROC and CMplots, saved random forest models, and saved stats objects. 

    usage: RF_whole_sequences_eval_CV.py  [-h]  [-o OUTDIR]  [train_file ]  [holdout_file]

CV sampled data using GroupShuffleSplit. The run will CV with associated
results: ROC and CMplots, saved models, saved stats objects

positional arguments:

  train_file:            Training dataset
  
  holdout_file:          Holdout dataset

optional arguments:

  -h, --help:            show this help message and exit
  
  -o OUTDIR, --outdir OUTDIR  Output dir: a default is provided

### RF_truncated_sequences_eval_CV.py

This Python script takes the matrix containing chromosome, plasmid and phage k-mer frequencies from step-6 and runs random forest model using the 5-fold cross-validation method but it tests the model with the truncated sequences.

It requires the path for training, testing and hold-out datasets with full sequences and path for the output directory. The output directory will contain following files:
ROC and CMplots, saved random forest models, and saved stats objects. 

    usage: RF_truncated_sequences_eval_CV.py [-h] [-o OUTDIR]  [train_file]  [test_file]  [hold_file]

CV of main and truncated data in parallel. The run will output a dict object
and the main figures: ROC_AUC and Conf.MTX

positional arguments:

  train_file:            Input file #1 containing counts from full genomes
  
  test_file:             Input file #1 containing counts from full genomes
  
  hold_file:             Input file #1 containing counts from full genomes

optional arguments:

  -h, --help            show this help message and exit
  
  -o OUTDIR, --outdir OUTDIR  Output dir: a default is provided

### RF_fragment_sequences_eval_CV.py

This Python script takes the matrix from step-6 including k-mer frequencies of 5000 nucleotide fragments for training and testing the random forest models. 

It requires the path for training and hold-out dataset, information for the number of sampling time, class weights and path for the output directory. The output directory will contain following files:
ROC and CMplots, saved random forest models, and saved stats objects. 

    usage: RF_fragment_sequences_eval_CV.py [-h] [-o OUTDIR]  [train_file]  [hold_file]  [spl_times]  [weighted]

CV sampled data using GroupShuffleSplit. The run will CV with associated
results: ROC and CMplots, saved models, saved stats objects

positional arguments:

  train_file:            Training dataset
  
  hold_file:             Holdout dataset
  
  spl_times:             Number of times the sequence was sampled <int>
  
  weighted:              "True"/"False": apply group weights <here we use 100:1:1 for Ch:Pl:Ph respectively, type=str>

optional arguments:

  -h, --help            show this help message and exit
  
  -o OUTDIR, --outdir OUTDIR  Output dir: a default is provided

## Post hoc analysis 

Prediction performances of machine learning models can be assessed with different performance assessment metrics including area under the curve (AUC), Matthews correlation
coefficient (MCC), F-score, confusion matrix. 

### CV_plotting_functions.py 

This program takes the dictionaries produced by the machine learning models and generated receiver operating characteristic (ROC) curves and confusion metrices. 

### plot_whole_seq_data.py

It has the same function with the CV_plotting_functions.py except specific to the whole fragments.

### plot_fragment_seq_data.py

It has the same function with the CV_plotting_functions.py except specific to the truncated sequences.

### get_ML_metrics.py

It assesses the machine learning prediction performances using AUC, MCC, F-score, and confusion matrix. 

### Who do I talk to? ###

* gvladja@gmail.com

### Required Python3 libraries ###

* CV_plotting_functions (available here: https://bitbucket.org/vlagri/oriclass/src/master/post_hoc_analysis/CV_plotting_functions.py)

* joblib

* scipy

* sklearn

* matplotlib.pyplot

* numpy

* pandas

* scikitplot

### Install KMC ###

KMC can be downloaded and installed from here: https://github.com/refresh-bio/KMC

## Downloads ##

The supplementary files (Aytan_Aktug_SourceFinder_Supp_Figures and Aytan_Aktug_SourceFinder_Supp_Tables)for the SourceFinder paper are available in downloads.