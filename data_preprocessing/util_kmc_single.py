#!/usr/bin/env python
# gvladja
# 19.01.21

import argparse

def kmc_single(infile, out_dir, k_size):
    """
    Kmerize a single sequence using KMC tool
    """
    import subprocess
    import sys
    import os
    import re
    # Sequences
    base_dir = os.path.dirname(os.path.realpath(infile))
    base_name = os.path.basename(infile)
    infile_1 = infile
    file_clean = re.sub("(\.fasta|\.fna)", '', base_name)
    outfile_1 = f'{out_dir}/{k_size}_mer/residual/{file_clean}'
    temp_file_1 = f'{out_dir}/{k_size}_mer/residual/temp'
    os.makedirs(temp_file_1, exist_ok=True)
    infile_2 = outfile_1
    outfile_2 = f'{out_dir}/{k_size}_mer/{file_clean}_{k_size}_kms'
    CMD = f'kmc -t8 -k{k_size} -fm -ci1 -cs1677215 {infile_1} {outfile_1} {temp_file_1} ; kmc_dump {infile_2} {outfile_2}'
    subprocess.run(CMD, shell=True)
    return f'{outfile_2}'


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='KMC: sub-sequence a sequence file into k-mers using a specified size')
    parser.add_argument('file_path', help='Path to the input file')
    parser.add_argument('output_dir', help='Path to the output directory')
    # To make the input integers
    parser.add_argument("kmer_size", help='Specify k-mer size')
    args = parser.parse_args()
    # CREATE A LIST OF ENTRIES
    kmc_single(args.file_path, args.output_dir, args.kmer_size)

