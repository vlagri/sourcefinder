#!/bin/usr/env python
# gvladja
# 19.01.21

def combine_shuffle_n_split_datasets(file_list, output_file, out_dir, seed=1, holdout_size=0.1):
    """
    Combine the 3 datasets together into a single dataframe,
    shuffle the records and split into
    training and holdout datasets.
    """
    import pandas as pd
    import os
    from sklearn.utils import shuffle
    from sklearn.model_selection import train_test_split
    import re
    os.makedirs(os.path.dirname(output_file), exist_ok=True)
    print("### Loading file list ###")
    with open(file_list) as f:
        files = f.read().splitlines()
    try:
        len(files) == 3
    except:
        print("Incorrect number of entries: expectecd 3x [Chromosomal, Plasmid, Phage]")
        print(files)
        os.exit()

    # Read files
    print("### Reading in the DFs ###")
    df1 = pd.read_csv(files[0], sep='\t', dtype = {"origin":int, "genome_id":str})
    df2 = pd.read_csv(files[1], sep='\t', dtype = {"origin":int, "genome_id":str})
    df3 = pd.read_csv(files[2], sep='\t', dtype = {"origin":int, "genome_id":str})

    # Assemble
    print("### Mergin DFs ###")
    df = pd.concat([df1,df2,df3], axis=0, ignore_index=True)
    # Free memory
    del df1, df2, df3

    # from the shuffle script
    file_path = output_file
    base_dir = os.path.dirname(file_path)
    base_name = os.path.basename(file_path)
    # Format
    fileout_main = re.sub('.tsv', '.train.tsv', base_name)
    fileout_hold = re.sub('.tsv', '.hold.tsv', base_name)
    df = shuffle(df, random_state=seed)
    # Split
    main, holdout = train_test_split(df, random_state=seed, test_size=holdout_size)
    # Make DIRs
    os.makedirs(os.path.dirname(f'{out_dir}/train/{fileout_main}'), exist_ok=True)
    os.makedirs(os.path.dirname(f'{out_dir}/hold/{fileout_hold}'), exist_ok=True)
    # Save
    main.to_csv(f'{out_dir}/train/{fileout_main}', sep="\t")
    holdout.to_csv(f'{out_dir}/hold/{fileout_hold}', sep="\t")

    # Save
    print("### Saving matrix ###")
    df.to_csv(output_file, sep="\t")

    return df


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Combining datasets')
    parser.add_argument("-f", "--file_list", help="List with the datasets\' paths")
    parser.add_argument("-o", "--output_file", help="Full path name of the output matrix file")
    parser.add_argument("-d", "--output_dir", help="Path to the output directory")
    parser.add_argument("-r", "--random_state", help="Random state value", default=1, type=int)
    parser.add_argument("-hs", "--holdout_size",help="Holdout size (float)", default=0.1, type=float)
    args = parser.parse_args()
    # CREATE A LIST OF ENTRIES
    combine_shuffle_n_split_datasets(args.file_list, args.output_file, args.output_dir, args.random_state, args.holdout_size )
