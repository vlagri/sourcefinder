#!/bin/usr/python3
# gvladja
# 19.01.21

import urllib.request
import concurrent.futures
import sys
import os
#import zipfil
#import time

def download_one(id_, to_dir):
    '''Retrieve a file from a PATRIC ftp url:
        <id_> the file name
        <to_dir> - STDOUT directory '''
    # PATRIC url
    url = "ftp://ftp.patricbrc.org/genomes/{}/{}.fna".format(id_, id_)
    urllib.request.urlretrieve(url, '{}/{}.fna'.format(to_dir, id_)) # STDOUT directory
    print(url, 2*" # # ", "{}/{}.fna".format(to_dir, id_))
    return url

def download(ids_file, to_dir):
    ''' Parallelization of the <download_one(id_, to_dir)> function:
        sending multiple requests to retrieve the files '''
    # Create a STDOUT DIR if does not exists
    if not os.path.exists(to_dir):
        os.makedirs(to_dir)

    with open(ids_file) as f:
        ids = f.read().splitlines()

    # PARALLEL loop
    with concurrent.futures.ProcessPoolExecutor(max_workers=5) as executor:
        future_to_url = {executor.submit(download_one, id_, to_dir): id_ for id_ in ids}

        # STDOUT
        for future in concurrent.futures.as_completed(future_to_url):
            url = future_to_url[future]
            try:
                data = future.result()
            except Exception as exc:
                print('%r generated an exception: %s' % (url, exc))
            else:
                print('%r file is %d bytes' % (url, len(data)))


    # END
    print(10*'#',"\nDownload is completed")

if __name__ == '__main__':
    download(sys.argv[1], sys.argv[2])
