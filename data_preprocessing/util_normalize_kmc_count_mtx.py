#!/bin/usr/env python
# gvladja
# 19.01.21

def normalizing_mtx_np(mtx_file, output_file, scaler, origin):
    """Processing the counts matrix: 
    normalizing each kmer count by its total nuber of kmers within the isolate
    and assigning the 'origins' label as a final column in the matrix"""
    import numpy as np
    import os
    # Make output directory
    os.makedirs(os.path.dirname(output_file), exist_ok=True)
    # Load
    mtx = np.load(mtx_file)
    
    # Get shape
    n_row, n_col = mtx.shape 
    
    # Normalize: devide each count by its total count per observation and scale
    mtx = mtx/mtx.sum(axis=1)[:,None]*int(scaler)
    
    # Add the labels
    #mtx = np.insert(mtx, n_col, int(origin), axis=1)
    mtx[:, n_col - 2] = [int(origin)]*n_row 

    np.save(output_file, mtx)

    return mtx

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Matrix normalization and class assignment')
    parser.add_argument("-m", "--mtx_file", help="Path to the input matrix file")
    parser.add_argument("-o", "--output_file", help="Full path name of the output matrix file")
    parser.add_argument("-s", "--scaler", help="A multiplication scaler: default=1000000")
    parser.add_argument("-g", "--origin", help="Level of the counts origin <int>: 1:Chromosome, 0:Plasmid, -1:Phage")
    args = parser.parse_args()
    # CREATE A LIST OF ENTRIES
    normalizing_mtx_np(args.mtx_file, args.output_file, args.scaler, args.origin)
