#!/bin/usr/env python
# gvladja
# 19.01.21

def prep_matrix(all_comb, inp_list, name):
    """Iterate over a list of all KMC files assemble 
    a matrix using numpy arrays:
    Input: <all_comb>: all kmer columns
           <inp_list>: file listing all kmc files to include (full path)
           <name>: file name to output
    Output: a single matrix of all kmc values for all isolates
    """
    import numpy as np
    import collections
    import os
    kmers = np.loadtxt(all_comb, dtype = str, delimiter = "\t")[0][1:]
        
    ins = np.loadtxt(inp_list, dtype = str, delimiter = "\t")

    mat = np.zeros((len(ins), len(kmers)), dtype=int)

    print(mat.shape)

    kmers_dict = collections.defaultdict(list)

    ##make a dict from all possible kmers

    for i in range(len(kmers)):
        kmers_dict[kmers[i]].append(i)

    for i in range(len(ins)):
        kms = np.loadtxt(ins[i], dtype = str, delimiter = "\t")
        for k in kms:
            mat[i, kmers_dict[k[0]]] = k[1]

    os.makedirs(os.path.dirname(name), exist_ok=True)
    np.save(name, mat)

    return mat

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Matrix preperation using pre-allocated memory space and dictionary iteration')
    parser.add_argument("-a", "--all_comb", help="File with all possible kmers")
    parser.add_argument("-i", "--input_list", help="Path to the list with KMC file paths")
    parser.add_argument("-o", "--output_file", help="Full path name of the output matrix file")
    args = parser.parse_args()
    # CREATE A LIST OF ENTRIES
    prep_matrix(args.all_comb, args.input_list, args.output_file)
