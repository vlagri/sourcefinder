#!/usr/bin/env python
# gvladja
# 19.01.21

import argparse

def fragments_extraction(infile, out_dir, size, number):
    from Bio import SeqIO
    import os
    import random
    """Read in a sequence file and truncate it randomly based on the specified size
    and output as separate files in the specified directory"""
    _name, _ext = os.path.splitext(os.path.basename(infile))
    dir_out = f'{out_dir}/{size}_bp_rand'
    os.makedirs(dir_out, exist_ok=True)
    try:
        try:
            record = SeqIO.read(infile, "fasta")
        except:
            # If file contains multiple entries: in VIRUS files some have mutliple contigs
            # but for this test, just taking the first contig will suffice
            records = SeqIO.parse(infile, "fasta")
            record = next(records)
        for spl in range(number):
            outfile = f'{out_dir}/{size}_bp_rand/{_name}_{size}_bp.spl{spl+1}{_ext}'
            # TAKE ALL YOU HAVE
            if size > len(record.seq):
                SeqIO.write(record[:], outfile, "fasta")
            else:    
                limit = len(record.seq)-size
                max_Ns = size-100
                exp = 0.9 # from 5000, decrease by 15% : in 25 iterations the tring should be ~100
                max_iter = 25
                iter = 1
                while max_iter >= iter:
                    start = random.randint(0,limit)
                    end = start+size
                    if record.seq[start:end].lower().count("n") < max_Ns:
                        SeqIO.write(record[start:end], outfile, "fasta")
                        break
                    else:
                        iter +=1     # iterate max 25 times when the non "n" string should be at least 100bp
                        max_Ns *=exp # decrease the maximum allowed gaps
    except Exception as err:
        print(err)
        print(f'### Could not truncate {record.id} ###')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='FUN: truncate sequence files from random sequence start to speficied length.\n The function returns a fasta record, with original extention, of truncated length. If raw sequence is shorter, the function will return everything in the entry. ')
    parser.add_argument("-f", "--file_path", help="Path to the input file")
    parser.add_argument("-o", "--output_dir", help="Path to the output directory")
    parser.add_argument("-s", "--size", help='Specify the sequence size', type=int)
    parser.add_argument("-n", "--number", help='Number of times to sample', type=int)
    args = parser.parse_args()
    # CREATE A LIST OF ENTRIES
    fragments_extraction(args.file_path, args.output_dir, args.size, args.number)
