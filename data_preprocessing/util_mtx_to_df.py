#!/bin/usr/env python
# gvladja
# 19.01.21

def npy_to_df(mtx_file, output_file, kmer_list, file_list):
    """ Append metadata to the 'bare' matrix, adding 'genome_id'
    and 'kmer' columns
    """
    import os 
    import numpy as np
    import pandas as pd

    # Make output path
    os.makedirs(os.path.dirname(output_file), exist_ok=True)

    # Load the mtx
    mtx = np.load(mtx_file)

    # kmers: extend by the label column "origin"
    kmers=np.genfromtxt(kmer_list, dtype=str)
    
    # List of kmc files: parce and extract the file names
    # (file suffix can be removed later)
    files=np.genfromtxt(file_list, dtype=str)
    all_files = [os.path.basename(file) for file in files]

    # Combine the 3 sources into a pandas DF
    df = pd.DataFrame(mtx, columns = kmers[0,1:], index = all_files)
    
    if "_5000_bp" in all_files[0]:  # This statement is only for fragments
        group_names = [name.split("_5000_bp")[0] for name in all_files]
        df["groups"] = group_names
    
    df.to_csv(output_file, sep="\t")
    return df


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description='"Annotate" matrix with kmer columns and isolate names')
    parser.add_argument('mtx_file', help='Path to the input matrix file')
    parser.add_argument('output_file', help='Path to the output file')
    # To make the input integers
    parser.add_argument("kmer_list", help='Path to the all kmers list')
    parser.add_argument("file_list", help='Path to the all files list')
    args = parser.parse_args()
    npy_to_df(args.mtx_file, args.output_file, args.kmer_list, args.file_list)
