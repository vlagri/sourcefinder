from Bio import SeqIO
import os
import random
import subprocess
import re
import numpy as np
import pandas as pd  
import collections
import sys
import shutil
import time
from joblib import dump, load
import datetime
import argparse

def sampling_seq_file_app(FILE, OUT_DIR, RANDOM_SEED, NUMBER):
    """
    Read in a sequence file and truncate it randomly 
    based on the specified size. Output as separate files 
    in the specified directory
    """

    _NAME, _EXT = os.path.splitext(os.path.basename(FILE))
    SIZE = 5000 # bp
    DIR_OUT = f'{OUT_DIR}'
    os.makedirs(DIR_OUT, exist_ok=True)
    try:
        try:
            record = SeqIO.read(FILE, "fasta")
        except:
            records = SeqIO.parse(FILE, "fasta")
            record = next(records)
        for spl in range(NUMBER):
            FILE_OUT = f'{OUT_DIR}/{_NAME}.frg{spl+1}{_EXT}'
            # TAKE ALL YOU HAVE
            if SIZE > len(record.seq):
                SeqIO.write(record[:], FILE_OUT, "fasta")
            else:    
                limit = len(record.seq)-SIZE
                start = random.randint(0,limit)
                end = start+SIZE
                SeqIO.write(record[start:end], FILE_OUT, "fasta")
    except Exception as err:
        print(err)
        print(f'### Could not truncate record: "{record.id}" ###')

def kmc_one_app(FILE_PATH, OUT_DIR, K_SIZE):
    """ 
    KMC application to a single sequence file:
    The function is a wrapper for kmc and kmc_dump calls.
    The function will produce a kmer frequency count
    for a given sequence of a given kmer size.
    Output directory provided will be appended with the kmer size
    """

    #########################################
    # Sequences
    BASE_DIR = os.path.dirname(os.path.realpath(FILE_PATH))
    BASE_NAME = os.path.basename(FILE_PATH)

    IN_FILE_1 = FILE_PATH
    FILE_CLEAN = re.sub("(\.fasta|\.fna)", '', BASE_NAME)
    OUT_FILE_1 = f'{OUT_DIR}/residual/{FILE_CLEAN}'
    TEMP_FILE_1 = f'{OUT_DIR}/residual/temp'
    os.makedirs(TEMP_FILE_1, exist_ok=True)
    IN_FILE_2 = OUT_FILE_1
    OUT_FILE_2 = f'{OUT_DIR}/{FILE_CLEAN}.{K_SIZE}_mer.kms'
    CMD = f'kmc -t8 -k{K_SIZE} -fm -ci1 -cs1677215 {IN_FILE_1} {OUT_FILE_1} {TEMP_FILE_1} ; kmc_dump {IN_FILE_2} {OUT_FILE_2}'
    subprocess.run(CMD, shell=True, stdout=open(os.devnull, 'wb'))
    return f'{OUT_FILE_2}'

def normalizing_mtx_app(mtx):
    """
    Normalizing the counts matrix:
    Each kmer devided by sum(kmers) for that entry
    """

    # STATIC
    scaler = 1000000    # reduce floating point precision
    # Get shape
    n_row, n_col = mtx.shape 
    # Normalize: devide each count by its total count per observation and scale
    mtx = mtx/mtx.sum(axis=1)[:,None]*int(scaler)
    # np.save(output_file, mtx)
    return mtx

def npy_to_df_app(mtx_file, output_file, kmer_list, file_list):
    """
    Convert the numpy matrix into a pandas DF.
    Saves and STOUT the DF matrix file
    """

    # Make output path
    os.makedirs(os.path.basename(output_file), exist_ok=True)
    # Load the mtx
    mtx = np.load(mtx_file)
    # kmers: extend by the label column "origin"
    kmers=np.genfromtxt(kmer_list, dtype=str)
    all_kmers = np.append(kmers, "origin")
    # List of kmc files: parce and extract the file names
    files=np.genfromtxt(file_list, dtype=str)
    all_files = [os.path.basename(file) for file in files]
    # Combine the 3 sources into a pandas DF
    df = pd.DataFrame(mtx, columns = all_kmers, index = all_files)
    df.to_csv(output_file, sep="\t")
    return df

def prep_matrix_app(kmers_list, file_list): #, out_file):
    """
    Iterate over a list of all KMC (kmer count) files and
    assembles a matrix using a model based template
    with precise kmer order
    """

    mat = np.zeros((len(file_list), len(kmers_list)), dtype=int)
    kmers_dict = collections.defaultdict(list)
    ##make a dict from all possible kmers
    for i in range(len(kmers_list)):
        kmers_dict[kmers_list[i]].append(i)
    for i in range(len(file_list)):
        kms = np.loadtxt(file_list[i], dtype = str, delimiter = "\t")
        for k in kms:
            mat[i, kmers_dict[k[0]]] = k[1]
    #os.makedirs(os.path.dirname(out_file), exist_ok=True)
    #np.save(out_file, mat)
    return mat

def ORIGIN_data_preprocessing(input_file, sampling_n = 1, seed = None):
    """
    Wrapper function for ORIGIN models input data preprocessing.
    The function takes a directory with sequence files (.fasta/.fna)
    and extracts a 5000 bp fragment at random (or everything available)
    samling the sequence <sampling_t> times. 
    Fragments are converted to kmers and combined together to form the
    input matrix to be used as input for the model predictions.
    Matrix is exported as a .tsv file in the <output_dir> directory.
    """

    # import datetime
    # DYNAMIC
    # date_stamp = datetime.datetime.today().strftime("%d_%h%y_%H%M%S")
    k_size = 5
    data_root = f'TeMp'
    path_frg = f'{data_root}/residual_files/fragments'
    path_kmc = f'{data_root}/residual_files/kmer_counts'
    kmc_temp = f'{path_kmc}/residual'
    install_path = os.path.dirname(os.path.realpath(__file__))
    # <<< necessary file, to arrange the columns in a correct order based on model
    template_file = f'{install_path}/models/frg_x10/cols_of_5_mer_kmc_agg.norm.mtx.hold.grp.tsv' 
    os.makedirs(path_frg, exist_ok=True)
    os.makedirs(path_kmc, exist_ok=True)
    sampling_seq_file_app(input_file, path_frg, seed, sampling_n)
    # CONVERT TO KMER COUNTS
    frg_list = os.listdir(path_frg)
    for frg in frg_list:
        kmc_one_app(f'{path_frg}/{frg}', path_kmc, k_size)
    shutil.rmtree(f'{kmc_temp}') # remove after conversion
    # LOAD THE KMER TEMPLATES
    columns = np.loadtxt(template_file, dtype = str, delimiter = "\t")
    kmers_list = list(columns[0][1:-2]) # The list comes with extra columns that can be removed from the template later
    # FULL PATH OF KMC FILES
    kmc_file_list = [os.path.join(path_kmc, f) for f in os.listdir(path_kmc)]
    raw_mtx = prep_matrix_app(kmers_list, kmc_file_list)
    norm_mtx = normalizing_mtx_app(raw_mtx)
    # all_files = [os.path.basename(f) for f in kmc_file_list]
    all_file_names = [os.path.basename(f).replace(".5_mer.kms", "") for f in kmc_file_list]
    df1 = pd.DataFrame(all_file_names, columns = ["genome_id"])
    df2 = pd.DataFrame(norm_mtx, columns = kmers_list)
    df = pd.concat([df1,df2], axis=1)
    shutil.rmtree(f'{data_root}') # remove after conversion
    return df

def ORIGIN_model_predictions(df, path):
    """
    Apply the ORIGIN project model on your preprocessed matrix for prediction
    of genomic class: chromosome, plasmid or phage.
    """
    # LOAD THE MODEL

    labels_table = {"1": "CH", "0": "PL", "-1": "PH"}
    labels_table2 = {"2": "CH", "1": "PL", "0": "PH"}
    df_genomeID = df.pop("genome_id")
    X_hat = df.values                             # convert to numpy array
    X_hat = np.nan_to_num(X_hat.astype(np.float32))
    columns=['genome_id', 'PH.prob',  'PL.prob', 'CH.prob', 'pred_class']
    results = pd.DataFrame(columns=columns)
    probs_all = []
    folds = 5
    
    print("#"*17)
    
    for fold in range(1,folds+1):
        print(f'Loading model {fold}/{folds}')
        # Computerome path to the model folder
        model_file = f'{path}/5_mer_kmc_agg_unWEIGHTED_CVfold_{fold}.joblib'    
        clf = load(f'{model_file}')
        for frg in range(df.shape[0]): 
            y_hat = clf.predict(X_hat)
            y_hat_prob = clf.predict_proba(X_hat)
            gID = df_genomeID[frg]
            probs = y_hat_prob[frg]                      # order: {-1: PH, 0: PL, 1: CH}
            probs_all.append(probs)
            pred = labels_table[str(y_hat[frg])] 
            record = pd.Series([gID, probs[0], probs[1], probs[2], pred], index=columns)
            # print(f'RECORD: {record}')
            results = results.append(record, ignore_index=True)
    
    print("#"*17)
    
    results = results.sort_values(['genome_id'])
    prob_mean = np.mean(probs_all, axis = 0)
    final_pred = labels_table2[str(prob_mean.argmax())]
    record = pd.Series(["MEAN", prob_mean[0], prob_mean[1], prob_mean[2], final_pred], index=columns)
    results = results.append(record, ignore_index=True)
    
    # shift index by +1 for 'easy read'
    results.index +=1
    print("RESULTS:")
    print('-'*60)
    print(results)
    print('-'*60)
    print(f'* CH = chromosome, PL = plasmid, PH = phage')

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(
        description='ORIGIN project: data preprocessing & ensemble model prediction pipeline. REQUIRES: a sequence file (.fna/.fasta)')
    parser.add_argument('input_file', help='Path to the sequence file to be analysed')
    parser.add_argument('-s','--seed', help='Random seed', type=int)
    parser.add_argument('-p', '--path', help='Path to the models')
    parser.add_argument('-n','--sampling_n', help='Sampling number', type=int, default = 1)
    args = parser.parse_args()
    
    random.seed(args.seed)
    
    # CREATE A LIST OF ENTRIES
    df = ORIGIN_data_preprocessing(input_file = args.input_file, sampling_n = args.sampling_n, seed = args.seed)
    ORIGIN_model_predictions(df, args.path)
    
    
    
