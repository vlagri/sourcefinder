# Date: 07.06.2021
# author: gvladja
# purpose: large-throughput-prediction using ORIclass models

from Bio import SeqIO
import os
import random
import subprocess
import re
import numpy as np
import pandas as pd
import collections
import sys
import shutil
import time
from joblib import dump, load
import datetime
import argparse

def sampling_seq_file_app(infile, out_dir, random_seed, number=1):
    """
    Read in a sequence file and truncate it randomly
    based on the specified size and output truncated file
    to a temporary directory
    """
    _name, _ext = os.path.splitext(os.path.basename(infile))
    SIZE = 5000  # bp
    os.makedirs(out_dir, exist_ok=True)
    try:
        try:
            record = SeqIO.read(infile, "fasta")
        except:
            # If file contains multiple entries: in VIRUS files some have mutliple contigs
            # but for this test, just taking the first contig will suffice
            records = SeqIO.parse(infile, "fasta")
            record = next(records)
        for spl in range(number):
            outfile = f'{out_dir}/{_name}.frg{spl+1}{_ext}'
            # TAKE ALL YOU HAVE
            if SIZE > len(record.seq):
                SeqIO.write(record[:], outfile, "fasta")
            else:
                limit = len(record.seq) - SIZE
                start = random.randint(0, limit)
                end = start + SIZE
                SeqIO.write(record[start:end], outfile, "fasta")
    except Exception as err:
        print(err)
        print(f'### NB! Record <{record.id}> could NOT be truncated! ###')


def kmc_one_app(file_path, out_dir, k_size):
    """
    KMC application to a single sequence file:
    The function is a wrapper for kmc and kmc_dump calls.
    The function will produce a kmer frequency count
    for a given sequence of a given kmer size.
    Output directory provided will be appended with the kmer size
    """
    #########################################
    # Sequences
    BASE_DIR = os.path.dirname(os.path.realpath(file_path))
    BASE_NAME = os.path.basename(file_path)
    IN_FILE_1 = file_path
    FILE_CLEAN = re.sub("(\.fasta|\.fna)", '', BASE_NAME)
    OUT_FILE_1 = f'{out_dir}/residual/{FILE_CLEAN}'
    TEMP_FILE_1 = f'{out_dir}/residual/temp'
    os.makedirs(TEMP_FILE_1, exist_ok=True)
    IN_FILE_2 = OUT_FILE_1
    OUT_FILE_2 = f'{out_dir}/{FILE_CLEAN}.{k_size}_mer.kms'
    CMD = f'kmc -t8 -k{k_size} -fm -ci1 -cs1677215 {IN_FILE_1} {OUT_FILE_1} {TEMP_FILE_1} ; kmc_dump {IN_FILE_2} {OUT_FILE_2}'
    subprocess.run(CMD, shell=True, stdout=open(os.devnull, 'wb'))
    return f'{OUT_FILE_2}'


def normalizing_mtx_app(mtx):
    """
    Normalizing the counts matrix:
    Each kmer devided by sum(kmers) for that entry
    """
    # STATIC
    scaler = 1000000    # reduce floating point precision
    # Get shape
    n_row, n_col = mtx.shape
    # Normalize: devide each count by its total count per observation and scale
    mtx = mtx / mtx.sum(axis=1)[:, None] * int(scaler)
    # np.save(output_file, mtx)
    return mtx


def npy_to_df_app(mtx_file, output_file, kmer_list, file_list):
    """
    Convert the numpy matrix into a pandas DF.
    Saves and STOUT the DF matrix file
    """
    # Make output path
    os.makedirs(os.path.basename(output_file), exist_ok=True)
    # Load the mtx
    mtx = np.load(mtx_file)
    # kmers: extend by the label column "origin"
    kmers = np.genfromtxt(kmer_list, dtype=str)
    all_kmers = np.append(kmers, "origin")
    # List of kmc files: parce and extract the file names
    files = np.genfromtxt(file_list, dtype=str)
    all_files = [os.path.basename(file) for file in files]
    # Combine the 3 sources into a pandas DF
    df = pd.DataFrame(mtx, columns=all_kmers, index=all_files)
    df.to_csv(output_file, sep="\t")
    return df


def prep_matrix_app(kmers_list, file_list):  # , out_file):
    """
    Iterate over a list of all KMC (kmer count) files and
    assembles a matrix using a model based template
    with precise kmer order
    """

    mat = np.zeros((len(file_list), len(kmers_list)), dtype=int)
    kmers_dict = collections.defaultdict(list)
    # make a dict from all possible kmers
    for i in range(len(kmers_list)):
        kmers_dict[kmers_list[i]].append(i)
    for i in range(len(file_list)):
        kms = np.loadtxt(file_list[i], dtype=str, delimiter="\t")
        for k in kms:
            mat[i, kmers_dict[k[0]]] = k[1]
    #os.makedirs(os.path.dirname(out_file), exist_ok=True)
    #np.save(out_file, mat)
    return mat


def ORIGIN_data_preprocessing(outm, input_dir, seed=None, sampling_n=1):
    """
    The function takes a directory with sequence files (.fasta/.fna)
    Whole sequences are converted to kmers and combined together to form the
    input matrix to be used as input for the model predictions.
    The function returns a pandas DataFrame with normalised kmer counts arranged
    in
    """
    # DYNAMIC
    k_size = 5
    data_root = f'{outm}/TeMp'
    path_frg = f'{data_root}/residual_files/fragments'
    path_kmc = f'{data_root}/residual_files/kmer_counts'
    kmc_temp = f'{path_kmc}/residual'
    install_path = os.path.dirname(os.path.realpath(__file__))
    os.makedirs(data_root, exist_ok=True)
    os.makedirs(path_frg, exist_ok=True)
    os.makedirs(path_kmc, exist_ok=True)
    # <<< necessary file, to arrange the columns in a correct order based on model
    template_file = f'{install_path}/models/frg_x10/cols_of_{k_size}_mer_kmc_agg.norm.mtx.hold.grp.tsv'
    fasta_file_list = os.listdir(input_dir)
    # Sample fragments
    for f in fasta_file_list:
        sampling_seq_file_app(f'{input_dir}/{f}', path_frg, seed, sampling_n)
    # GET A LIST OF FRAGMENT FILES in input_dir
    frg_file_list = os.listdir(path_frg)
    for frg in frg_file_list:
        kmc_one_app(f'{path_frg}/{frg}', path_kmc, k_size)
    shutil.rmtree(f'{kmc_temp}')  # remove after conversion
    # LOAD THE KMER TEMPLATES
    columns = np.loadtxt(template_file, dtype=str, delimiter="\t")
    # The list comes with extra columns that can be removed from the template later
    kmers_list = list(columns[0][1:-2])
    # FULL PATH OF KMC FILES
    kmc_file_list = [os.path.join(path_kmc, f) for f in os.listdir(path_kmc)]
    raw_mtx = prep_matrix_app(kmers_list, kmc_file_list)
    norm_mtx = normalizing_mtx_app(raw_mtx)
    # all_files = [os.path.basename(f) for f in kmc_file_list]
    all_file_names = [os.path.basename(f).split("_mer")[0][:-2] for f in kmc_file_list]
    df1 = pd.DataFrame(all_file_names, columns=["genome_id"])
    df2 = pd.DataFrame(norm_mtx, columns=kmers_list)
    df = pd.concat([df1, df2], axis=1)
    shutil.rmtree(f'{data_root}')  # remove after conversion
    return df


def ORIGIN_model_predictions(df, path):
    """
    Apply the ORIGIN project model on your preprocessed matrix for prediction
    of genomic class: chromosome, plasmid or phage.
    """
    # LOAD THE MODEL

    labels_table = {"1": "CH", "0": "PL", "-1": "PH"}
    labels_table2 = {"2": "CH", "1": "PL", "0": "PH"}
    df_genomeID = df.pop("genome_id")
    total_count = len(df_genomeID)
    X_hat = df.values                             # convert to numpy array
    X_hat = np.nan_to_num(X_hat.astype(np.float32))
    columns = ['genome_id', 'PH.prob',  'PL.prob', 'CH.prob']
    results_all = pd.DataFrame(columns=columns)
    probs_all = []
    folds = 5
    for fold in range(1, folds + 1):
        model_file = f'{path}/5_mer_kmc_agg_unWEIGHTED_CVfold_{fold}.joblib'
        print(f'Loading model {fold}/{folds}')
        clf = load(f'{model_file}')      
        y_hat_prob = clf.predict_proba(X_hat)
        probs = y_hat_prob
        probs_all.append(probs)
   
    for i in range(total_count):
    
        tmp_res = []
    
        for p in range(len(probs_all)):
        
            tmp_res.append(probs_all[p][i])
        
        record = pd.Series([df_genomeID[i], np.mean(tmp_res, axis = 0)[0], np.mean(tmp_res, axis = 0)[1], np.mean(tmp_res, axis = 0)[2]], index=columns)
       
        results_all = results_all.append(record, ignore_index=True)

    results_all[['id', 'frag']] = results_all["genome_id"].str.split(".frg", expand=True)
    
    results = results_all[['id', 'frag', 'PH.prob',  'PL.prob', 'CH.prob']]
    
    results = results.groupby("id").mean()
    
    results["agg.label"] = results.idxmax(axis="columns").str.split(".prob").str[0]

    results = results.reset_index()
    
    results = results.round(3)
    
    results.index += 1

    return results


if __name__ == '__main__':

    date_stamp = datetime.datetime.today().strftime("%Y%h%d_%H%M%S")
    parser = argparse.ArgumentParser(
        description='ORIGIN project: data preprocessing & ensemble model prediction pipeline. REQUIRES: a directory with sequence files (.fna/.fasta)')
    parser.add_argument(
        'input_dir', help='Path to the directory with sequence files to be analysed')
    parser.add_argument('-s', '--seed', help='Random seed')
    parser.add_argument('-p', '--path', help='Path to the models')
    parser.add_argument('-n', '--sampling_n',
                        help='Number of fragments to analyse', type=int, default=1)
    parser.add_argument('-o', '--output', help='Output directory', default='.')
    args = parser.parse_args()
    
    random.seed(args.seed)
    
    # CREATE A LIST OF ENTRIES
    df = ORIGIN_data_preprocessing(outm=args.output, input_dir=args.input_dir, seed=args.seed, sampling_n=args.sampling_n)
    results = ORIGIN_model_predictions(df, args.path)
    seed, sampling = args.seed, args.sampling_n
    if args.seed == None:
        seed = "rand"
    # if args.sampling_n == None:
    #     sampling = 1
    os.makedirs(args.output, exist_ok=True)
    file_name = f'{args.output}/{date_stamp}_oriclass_s{seed}_n{sampling}.tsv'
    results.to_csv(file_name, sep='\t', index=False)
    
    
