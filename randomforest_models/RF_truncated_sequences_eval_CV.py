#!/usr/bin/env python
# gvladja
# 02.12.20

# %% LIBRARIES
# general purpose
import argparse
import time
import random
from itertools import cycle
import numpy as np
import pickle
#from joblib import dump, load # For the model persistance
import os
import pandas as pd
import datetime
import shutil
import matplotlib.pyplot as plt
from CV_plotting_functions import *
# SKLEARN
from sklearn.model_selection import train_test_split, KFold  # GroupShuffleSplit
from sklearn.metrics import classification_report, confusion_matrix, multilabel_confusion_matrix,\
    accuracy_score, roc_auc_score, matthews_corrcoef, plot_confusion_matrix,\
    ConfusionMatrixDisplay, plot_roc_curve, auc, accuracy_score, f1_score, roc_curve, SCORERS, make_scorer
#from sklearn.inspection import permutation_importance

# MODELS
# Remember to add them to the <models> variable
# and initiate in the %% ITERATE: MODELS loop
from sklearn.ensemble import RandomForestClassifier


def ML_TRUNC_eval_CV(train_file, test_file, hold_file, outdir):
    date_stamp = datetime.datetime.now().strftime("%d_%m_%Y")  # _%H%M%S")
    print(date_stamp)
    # %% RANDOMNESS FIXING
    time_start = time.time()
    # Set a seed value
    seed_value = 1
    # 1. Set `PYTHONHASHSEED` environment variable at a fixed value
    os.environ['PYTHONHASHSEED'] = str(seed_value)
    # 2. Set `python` built-in pseudo-random generator at a fixed value
    random.seed(seed_value)
    # 3. Set `numpy` pseudo-random generator at a fixed value
    np.random.seed(seed_value)
    # INITIATE DATASTORE
    tests = ["mcc", "acc", "f1_score", "roc_auc",
                "y_pred", "y_score", "y_test", "cm"]
    models = ["RF"]
    params = ["full", "trunc"]
    datasets = ["test", "hold"]
    stats = {}
    for model in models:
        for param in params:
            for data in datasets:
                for test in tests:
                    if data == "test":
                        stats[f'{model}_{param}_{data}_{test}'] = []
                    else:
                        stats[f'{model}_trunc_{data}_{test}'] = []
    # FITTING TIME ARR
    for model in models:
        for param in params:
            stats[f'{model}_{param}_fit_time_s'] = []
    # %% FILENAMES
    train_dir_1 = os.path.dirname(os.path.abspath(train_file))
    train_dir_2 = os.path.dirname(os.path.abspath(test_file))
    outfile_base = os.path.basename(train_file).split(".")[0]
    k_size = os.path.basename(train_file).split("_")[0]
    # FILES TO READ
    for folder in ["stats", "figures"]:
        os.makedirs(f'{outdir}/{folder}', exist_ok=True)

    infile_dir_1 = os.path.dirname(os.path.abspath(train_file))
    outfile_base_1 = os.path.basename(train_file).split(".")[0]
    infile_dir2 = os.path.dirname(os.path.abspath(test_file))
    outfile_base2 = os.path.basename(test_file).split(".")[0]
    # GENERAL VARS
    labels = [1, 0, -1]
    labels_names = ["Ch", "Pl", "Ph"]
    labels_table = {"1": "Ch", "0": "Pl", "-1": "Phage"}
    print(f'Files compared:\nTraining: {train_file}\nTruncated: {test_file}\nHoldout: {hold_file}')
    print(f'Output dir:\n{outdir}\n')

    # %% LOAD
    df_train = pd.read_csv(train_file, sep="\t",dtype = {"Unnamed: 0.1":str})
    # remove indexing from shuffling
    df_train.pop('Unnamed: 0')
    df_train.pop('groups')
    df_train_genomeID = df_train.pop("Unnamed: 0.1")   # remove "IDs: __str__"
    y = df_train.pop('origin')                      # remove 'labels'
    y = y.values                                    # convert to numpy array
    X_train_features = df_train.columns
    X = df_train.values                             # convert to numpy array
    X = np.nan_to_num(X.astype(np.float32))

    df_test = pd.read_csv(test_file, sep="\t",dtype = {"Unnamed: 0.1":str})
    # remove indexing from shuffling
    df_test.pop('Unnamed: 0')
    df_test.pop('groups')
    df_test_genomeID = df_test.pop("Unnamed: 0.1")   # remove "IDs: __str__"
    y_hat = df_test.pop('origin')                      # remove 'labels'
    y_hat = y_hat.values                                    # convert to numpy array
    X_hat_features = df_test.columns
    X_hat = df_test.values                             # convert to numpy array
    X_hat = np.nan_to_num(X_hat.astype(np.float32))

    df_hold = pd.read_csv(hold_file, sep="\t",dtype = {"Unnamed: 0.1":str})
    # remove indexing from shuffling
    df_hold.pop('Unnamed: 0')
    df_hold.pop("groups")
    df_hold_genomeID = df_hold.pop("Unnamed: 0.1")     # remove "IDs: __str__"
    y_h = df_hold.pop('origin')                     # remove 'labels'
    y_h = y_h.values                                # convert to numpy array
    X_hold_features = df_hold.columns
    X_h = df_hold.values                            # convert to numpy array
    X_h = np.nan_to_num(X_h.astype(np.float32))
    # %% CHECK ORDER: order is correct but there was some "float spill" and nothing I do seem to change anything ... but it would appear theat the order is correct
    # y and y_hat should be the same but
    # X and X_hat should be different except for the "genome_id"s that should match
    # if not df_train_genomeID.equals(df_test_genomeID):
    #     print(f'### The order of the records is NOT MATHCING! ###\n### EXITING ###')
    #     print(df_train_genomeID.head())
    #     print(df_test_genomeID.head())
    #     raise SystemExit
    # print(f'### The order of the records is matching! ###')
    # %% ML SPECIFIC VARIABLES
    params_datasets = [(X, y), (X_hat, y_hat)]  # data types: full and truncated
    model_list = ["RF"]  # , "KNC", "MLPC"]
    total_fold = 5
    random_state = 1
    n_jobs = -1
    CV = KFold(n_splits=total_fold, shuffle = True, random_state=random_state)
    print(date_stamp)
    print(f'{"-"*20}')
    print(*[i + ":\n" for i in stats])
    print(f'{"-"*20}')
    # %% ITERATE: MODELS
    # INITIATE
    for model in models:
        if model == "RF":
            # RandomForest
            model_name = f"Random Forest: {params}"
            clf = RandomForestClassifier(n_jobs=n_jobs, random_state=random_state, bootstrap=False)
            # chosen from RandomSearch Analysis
            clf_params = {'n_estimators': 800, 'max_depth': 400}
            clf.set_params(**clf_params)
            print(f'{model_name}')
            print(f'{"-"*20}')
            print(clf)
            print(f'{"-"*20}')
            # ITERATE: folds and get individual results
        for fold, (train, test) in enumerate(CV.split(X, y)):
            # MODEL TRAIN
            time_srt = time.time()
            clf.fit(X[train], y[train])
            time_delta = time.time() - time_srt
            stats[f'{model}_{param}_fit_time_s'].append(time_delta)
            for i, param in enumerate(params):
                db_X, db_y = params_datasets[i] # Testing against FULL X and Y test split or X_hat, y_hat from TRUNC
                # MODEL TEST
                stats[f'{model}_{param}_test_y_test'].append(db_y[test])
                stats[f'{model}_{param}_test_y_pred'].append(
                    clf.predict(db_X[test]))
                stats[f'{model}_{param}_test_y_score'].append(
                    clf.predict_proba(db_X[test]))
                stats[f'{model}_{param}_test_mcc'].append(matthews_corrcoef(
                    db_y[test], stats[f'{model}_{param}_test_y_pred'][fold]))
                stats[f'{model}_{param}_test_acc'].append(accuracy_score(
                    db_y[test], stats[f'{model}_{param}_test_y_pred'][fold]))
                stats[f'{model}_{param}_test_f1_score'].append(f1_score(
                    db_y[test], stats[f'{model}_{param}_test_y_pred'][fold], average='macro'))
                stats[f'{model}_{param}_test_roc_auc'].append(roc_auc_score(
                    db_y[test], stats[f'{model}_{param}_test_y_score'][fold], multi_class="ovr"))
                stats[f'{model}_{param}_test_cm'].append(confusion_matrix(
                    db_y[test], stats[f'{model}_{param}_test_y_pred'][fold], labels=labels, normalize="true"))
            # EVALUATE: HOLDOUT
            stats[f'{model}_trunc_hold_y_test'].append(y_h)
            stats[f'{model}_trunc_hold_y_pred'].append(
                clf.predict(X_h))
            stats[f'{model}_trunc_hold_y_score'].append(
                clf.predict_proba(X_h))
            stats[f'{model}_trunc_hold_mcc'].append(matthews_corrcoef(
                y_h, stats[f'{model}_trunc_hold_y_pred'][fold]))
            stats[f'{model}_trunc_hold_acc'].append(accuracy_score(
                y_h, stats[f'{model}_trunc_hold_y_pred'][fold]))
            stats[f'{model}_trunc_hold_f1_score'].append(f1_score(
                y_h, stats[f'{model}_trunc_hold_y_pred'][fold], average='macro'))
            stats[f'{model}_trunc_hold_roc_auc'].append(roc_auc_score(
                y_h, stats[f'{model}_trunc_hold_y_score'][fold], multi_class="ovr"))
            stats[f'{model}_trunc_hold_cm'].append(confusion_matrix(
                y_h, stats[f'{model}_trunc_hold_y_pred'][fold], labels=labels, normalize="true"))
            print(f'### Fold #{fold+1} is complete ###')
        # SAVE DIC object
        object_name1 = f'{outdir}/stats/{k_size}_mer_{model}_TRUNC_seq_stats_object.pkl'
        with open(object_name1, "wb") as pkl_handle1:
            pickle.dump(stats, pkl_handle1)
        # # LOAD
        # with open(f'{outdir}/{object_name}', "rb") as pkl_handle:
        #     stats = pickle.load(pkl_handle)
    lines = ["--","-","-.",":"]
    linecycler = cycle(lines)
    for model in models:
        for param in params:       
            exp1 = f'{model}_ROC_{param}_{k_size}_CV{total_fold}.svg'
            exp2 = f'{model}_CM_{param}_{k_size}_CV{total_fold}.svg'
            fig, ax = plt.subplots(figsize=(6,5))
            for i in range(total_fold):
                linestyle=next(linecycler)
                my_plot_roc_curve(stats[f"{model}_{param}_test_y_test"][i], stats[f"{model}_{param}_test_y_score"][i],curves=('macro'), fold=i, linestyle=linestyle, ax=ax)
            mean_ROCauc = np.mean(stats[f"{model}_{param}_test_roc_auc"]).round(4)*100
            plt.title(f'{model}: {param} sequences | kmer = {k_size}\nCV accuracy: {mean_ROCauc:.2f}%',fontname='Times New Roman', fontsize=18, fontweight='bold')
            plt.xlabel("False Positive Rate", labelpad=10)
            plt.ylabel("True Positive Rate")
            plt.savefig(f'{outdir}/figures/{outfile_base}_{exp1}', bbox_inches='tight')
            
            mtx = my_avg_confusion_matrix(stats[f'{model}_{param}_test_cm'])
            plot_my_mini_confusion_matrix(mtx, labels=labels_names)
            plt.title(f'{model}: {param} sequences | kmer = {k_size}\nCV performance', fontname='Times New Roman', fontsize=18, fontweight='bold')
            plt.savefig(f'{outdir}/figures/{outfile_base}_{exp2}', bbox_inches='tight')
            # plt.show()
    # AVERAGE HOLDOUT RESULTS 
    for model in models:
        for param in params:
            for test in tests:
                stats[f'{model}_trunc_hold_{test}_avg'] = np.mean(stats[f'{model}_trunc_hold_{test}'], axis=0)
        # PLOT: ROC AUC
        exp3 = f'{model}_ROC_trunc_{k_size}_hold.svg'
        exp4 = f'{model}_CM_trunc_{k_size}_hold.svg'  
        mean_ROCauc_hold = np.mean(stats[f"{model}_trunc_hold_roc_auc_avg"]).round(4)*100
        skplt.metrics.plot_roc(stats[f"{model}_trunc_hold_y_test"][0], stats[f"{model}_trunc_hold_y_score_avg"])
        plt.title(f'{model}: trunc sequences | kmer = {k_size}\nHoldout accuracy: {mean_ROCauc_hold:.2f}%', fontname='Times New Roman', fontsize=18, fontweight='bold')  
        plt.savefig(f'{outdir}/figures/{outfile_base}_{exp3}', bbox_inches='tight')
        # PLOT: CONFUSION MATRIX
        mtx = my_avg_confusion_matrix(stats[f'{model}_trunc_test_cm'])
        plot_my_mini_confusion_matrix(mtx, labels=labels_names)
        plt.title(f'{model}: trunc sequences | kmer={k_size}\nHoldout performance', fontname='Times New Roman', fontsize=18, fontweight='bold')
        plt.savefig(f'{outdir}/figures/{outfile_base}_{exp4}', bbox_inches='tight')
    return(f'### CV is complete: {train_file} ###')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='CV of main and truncated data in parallel. The run will output a dict object and the main figures: ROC_AUC and Conf.MTX')
    parser.add_argument(
        'train_file', help='Input file #1 containing counts from full genomes')
    parser.add_argument(
        'test_file', help='Input file #1 containing counts from full genomes')
    parser.add_argument(
        'hold_file', help='Input file #1 containing counts from full genomes')
    parser.add_argument('-o', '--outdir', default=f'/home/projects/cge/people/vlagri/ORIGIN_project/1_data/6_results_ML',
                        help='Output dir: a default is provided')
    args = parser.parse_args()
    ML_TRUNC_eval_CV(args.train_file, args.test_file, args.hold_file, args.outdir)
