#!/usr/bin/env python
# gvladja
# 04.12.20

# %% LIBRARIES
# general purpose
import argparse
import time
import random
from itertools import cycle
import numpy as np
import pickle
from joblib import dump, load
import os
import pandas as pd
import datetime
import shutil
import matplotlib.pyplot as plt
from CV_plotting_functions import *
# SKLEARN
from sklearn.inspection import permutation_importance
from sklearn.model_selection import train_test_split, KFold, GroupShuffleSplit
from sklearn.metrics import classification_report, confusion_matrix, multilabel_confusion_matrix,\
    accuracy_score, roc_auc_score, matthews_corrcoef, plot_confusion_matrix,\
    ConfusionMatrixDisplay, plot_roc_curve, auc, accuracy_score, f1_score, roc_curve, SCORERS, make_scorer
# MODELS
# Remember to add them to the <models> variable
# and initiate in the %% ITERATE: MODELS loop
from sklearn.ensemble import RandomForestClassifier

def ML_SPL_seq_CV_group(train_file, hold_file, spl_times, outdir, weighted=False):
    date_stamp = datetime.datetime.now().strftime("%d_%m_%Y") 
    print(date_stamp)
    # %% RANDOMNESS FIXING
    time_start = time.time()
    # Set a seed value
    random_state = 1 
    # 1. Set `PYTHONHASHSEED` environment variable at a fixed value
    os.environ['PYTHONHASHSEED']=str(random_state)
    # 2. Set `python` built-in pseudo-random generator at a fixed value
    random.seed(random_state)
    # 3. Set `numpy` pseudo-random generator at a fixed value
    np.random.seed(random_state)
    # INITIATE DATASTORE
    tests = ["mcc", "acc", "f1_score", "roc_auc","y_pred", "y_score", "y_test", "cm"]
    models = ["RF"]
    params = [f"splx{spl_times}"]
    datasets = ["test", "hold"]
    stats = {}
    for model in models:
        for param in params:
            for data in datasets:
                for test in tests:
                    stats[f'{model}_{param}_{data}_{test}'] = []
    # FITTING TIME ARR                
    for model in models:
        for param in params:
            stats[f'{model}_{param}_fit_time_s']=[]
    # %% FILENAMES
    train_dir = os.path.dirname(os.path.abspath(train_file))
    outfile_base = os.path.basename(train_file).split(".")[0]
    k_size = os.path.basename(train_file).split("_")[0]
    for folder in ["stats","figures"]:
        os.makedirs(f'{outdir}/{folder}', exist_ok=True) 
    # %% LABELS 
    labels = [1, 0, -1]
    labels_names = ["Ch", "Pl", "Ph"]
    labels_table = {"1": "Ch", "0": "Pl", "-1": "Phage"}
    weights = {1:0.98,0:0.0098,-1:0.0098}
    print(f'Files compared:\nTraining:{train_file}\nHoldout:{hold_file}\n')
    print(f'Output dir:\n{outdir}\n')
    # %% LOAD
    print("TRAINING DATA IS LOADING")
    df_train = pd.read_csv(train_file, sep="\t", dtype={"groups":object})
    print(df_train)
    print(f'COLUMNS:\n{df_train.columns}')
    #df_train.pop('Unnamed: 0')                     # remove indexing from shuffling
    df_train_groups=df_train.pop("groups")
    print(f'GROUPS:\n{df_train_groups}')
    print(f'Counts of NANs in the df_train_groups: {df_train_groups.isnull().sum()}')
    df_train.pop('Unnamed: 0') 
    df_train_genomeID = df_train.pop("Unnamed: 0.1")   # remove "IDs: __str__"

    y = df_train.pop('origin')                      # remove 'labels'
    y = y.values                                    # convert to numpy array
    X_train_features = df_train.columns
    X = df_train.values                             # convert to numpy array
    X = np.nan_to_num(X.astype(np.float32))
    print("TRAINING DATA IS LOADED")
    df_hold = pd.read_csv(hold_file, sep="\t", dtype={"groups":str})
    df_hold.pop('Unnamed: 0')                            # remove indexing from shuffling
    df_hold_groups=df_hold.pop("groups")
    df_hold_genomeID = df_hold.pop("Unnamed: 0.1")     # remove "IDs: __str__"
    y_h = df_hold.pop('origin')                     # remove 'labels'
    y_h = y_h.values                                # convert to numpy array
    X_hold_features = df_hold.columns
    X_h = df_hold.values                            # convert to numpy array
    X_h = np.nan_to_num(X_h.astype(np.float32))
    print("HOLDOUT DATA IS LOADED")    
    # %% ML SPECIFIC VARIABLES
    total_fold = 5
    train_size = 0.8
    n_jobs = -1
    GSS_CV = GroupShuffleSplit(n_splits=total_fold, train_size=train_size, random_state=random_state)
    print(f'{"-"*20}')
    print(*[i + ":\n" for i in stats])
    print(f'{"-"*20}')
    # %% ITERATE: MODELS
    # INITIATE
    for model in models:
        for param in params:
            # %% RF parameters
            if model == "RF":
                model_name = f"Random Forest: {param}"
                clf = RandomForestClassifier(n_jobs=n_jobs, random_state=random_state, bootstrap=False)
                if weighted == "False":
                    class_weight = None
                    w_tag = " | unW"
                    w_tag_name = "unWEIGHTED"
                elif weighted == "True":
                    class_weight = weights
                    w_tag = " |  W "
                    w_tag_name = "WEIGHTED"
                clf_params = {'n_estimators': 800, 'max_depth': 400, "class_weight":class_weight}
                clf.set_params(**clf_params)
                print(f'{model_name}')
                print(f'{"-"*20}')
                print(clf)
                print(f'{"-"*20}')
            # ITERATE: folds and get individual results 
            for fold, (train, test) in enumerate(GSS_CV.split(X, y, df_train_groups)):
                # MODEL TRAIN
                time_srt = time.time()
                clf.fit(X[train], y[train])
                time_delta = time.time() - time_srt
                stats[f'{model}_{param}_fit_time_s'].append(time_delta)
                # EVALUATE: TEST
                stats[f'{model}_{param}_test_y_test'].append(y[test])
                stats[f'{model}_{param}_test_y_pred'].append(
                    clf.predict(X[test]))
                stats[f'{model}_{param}_test_y_score'].append(
                    clf.predict_proba(X[test]))
                stats[f'{model}_{param}_test_mcc'].append(matthews_corrcoef(
                    y[test], stats[f'{model}_{param}_test_y_pred'][fold]))
                stats[f'{model}_{param}_test_acc'].append(accuracy_score(
                    y[test], stats[f'{model}_{param}_test_y_pred'][fold]))
                stats[f'{model}_{param}_test_f1_score'].append(f1_score(
                    y[test], stats[f'{model}_{param}_test_y_pred'][fold], average='macro'))
                stats[f'{model}_{param}_test_roc_auc'].append(roc_auc_score(
                    y[test], stats[f'{model}_{param}_test_y_score'][fold], multi_class="ovr"))
                stats[f'{model}_{param}_test_cm'].append(confusion_matrix(
                    y[test], stats[f'{model}_{param}_test_y_pred'][fold], labels=labels, normalize="true"))
                # EVALUATE: HOLDOUT
                stats[f'{model}_{param}_hold_y_test'].append(y_h)
                stats[f'{model}_{param}_hold_y_pred'].append(
                    clf.predict(X_h))
                stats[f'{model}_{param}_hold_y_score'].append(
                    clf.predict_proba(X_h))
                stats[f'{model}_{param}_hold_mcc'].append(matthews_corrcoef(
                    y_h, stats[f'{model}_{param}_hold_y_pred'][fold]))
                stats[f'{model}_{param}_hold_acc'].append(accuracy_score(
                    y_h, stats[f'{model}_{param}_hold_y_pred'][fold]))
                stats[f'{model}_{param}_hold_f1_score'].append(f1_score(
                    y_h, stats[f'{model}_{param}_hold_y_pred'][fold], average='macro'))
                stats[f'{model}_{param}_hold_roc_auc'].append(roc_auc_score(
                    y_h, stats[f'{model}_{param}_hold_y_score'][fold], multi_class="ovr"))
                stats[f'{model}_{param}_hold_cm'].append(confusion_matrix(
                    y_h, stats[f'{model}_{param}_hold_y_pred'][fold], labels=labels, normalize="true"))
                print(f'### {model}: Fold #{fold+1} is complete ###')
    # SAVE DIC object
    object_name1 = f'{outdir}/stats/{k_size}_mer_{model}_SPL_x{spl_times}_{w_tag_name}_stats_object.pkl'
    with open(object_name1, "wb") as pkl_handle1:
        pickle.dump(stats, pkl_handle1)
    # # LOAD
    # with open(f'{outdir}/{object_name}', "rb") as pkl_handle:
    #     stats = pickle.load(pkl_handle)
    # %% MAKING SENSE OF IT ALL
    lines = ["--","-","-.",":"]
    linecycler = cycle(lines)
    for model in models:
        for param in params:       
            exp1 = f'{model}_ROC_{param}_CV{total_fold}_{w_tag_name}.svg'
            exp2 = f'{model}_CM_{param}_CV{total_fold}_{w_tag_name}.svg'
            fig, ax = plt.subplots(figsize=(6,5))
            for i in range(total_fold):
                linestyle=next(linecycler)
                my_plot_roc_curve(stats[f"{model}_{param}_test_y_test"][i], stats[f"{model}_{param}_test_y_score"][i],curves=('macro'), fold=i, linestyle=linestyle, ax=ax)
            mean_ROCauc = np.mean(stats[f"{model}_{param}_test_roc_auc"]).round(4)*100
            plt.title(f'{model}: 5000bp x{spl_times}{w_tag} | kmer = {k_size}\nCV accuracy: {mean_ROCauc:.2f}%',fontname='Times New Roman', fontsize=18, fontweight='bold')
            plt.xlabel("False Positive Rate", labelpad=10)
            plt.ylabel("True Positive Rate")
            plt.savefig(f'{outdir}/figures/{outfile_base}_{exp1}', bbox_inches='tight')
            
            mtx = my_avg_confusion_matrix(stats[f'{model}_{param}_test_cm'])
            plot_my_mini_confusion_matrix(mtx, labels=labels_names)
            plt.title(f'{model}: 5000bp x{spl_times}{w_tag} | kmer = {k_size}\nCV performance', fontname='Times New Roman', fontsize=18, fontweight='bold')
            plt.savefig(f'{outdir}/figures/{outfile_base}_{exp2}', bbox_inches='tight')
            # plt.show()

    # AVERAGE HOLDOUT RESULTS 
    for model in models:
        for param in params:
            for test in tests:
                stats[f'{model}_{param}_hold_{test}_avg'] = np.mean(stats[f'{model}_{param}_hold_{test}'], axis=0)
        # PLOT: ROC AUC
        exp3 = f'{model}_ROC_{param}_{k_size}_hold_{w_tag_name}.svg'
        exp4 = f'{model}_CM_{param}_{k_size}_hold_{w_tag_name}.svg'  
        mean_ROCauc_hold = np.mean(stats[f"{model}_{param}_hold_roc_auc_avg"]).round(4)*100
        skplt.metrics.plot_roc(stats[f"{model}_{param}_hold_y_test"][0], stats[f"{model}_{param}_hold_y_score_avg"])
        plt.title(f'{model}: 5000bp x{spl_times}{w_tag} | kmer = {k_size}\nHoldout accuracy: {mean_ROCauc_hold:.2f}%', fontname='Times New Roman', fontsize=18, fontweight='bold')
        plt.savefig(f'{outdir}/figures/{outfile_base}_{exp3}', bbox_inches='tight')
        # PLOT: CONFUSION MATRIX
        mtx = my_avg_confusion_matrix(stats[f'{model}_{param}_hold_cm'])
        plot_my_mini_confusion_matrix(mtx, labels=labels_names)
        plt.title(f'{model}: 5000bp x{spl_times}{w_tag} | kmer={k_size}\nHoldout performance', fontname='Times New Roman', fontsize=18, fontweight='bold')
        plt.savefig(f'{outdir}/figures/{outfile_base}_{exp4}', bbox_inches='tight')
    return(f'### CV is complete: {train_file} ###')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='CV sampled data using GroupShuffleSplit. \
            The run will CV with associated results: ROC and CMplots, saved models, saved stats objects')
    parser.add_argument('train_file', help='Training dataset')
    parser.add_argument('hold_file', help='Holdout dataset')
    parser.add_argument('spl_times', help='Number of times the sequence was sampled <int>')
    parser.add_argument('outdir', help='Output dir: a default is provided')
    parser.add_argument('-w','--weighted', help='"True"/"False": apply group weights <here we use 100:1:1 for Ch:Pl:Ph respectively, type=str>', type=str)
    args = parser.parse_args()
    # CREATE A LIST OF ENTRIES
    ML_SPL_seq_CV_group(args.train_file, args.hold_file,  args.spl_times, args.outdir, args.weighted)
