from CV_plotting_functions import *
import pickle
spl_times = 10
tests = ["mcc", "acc", "f1_score", "roc_auc","y_pred", "y_score", "y_test", "cm"]
models = ["RF"]
datasets = ["test", "hold"]
total_fold = 5
# %% LABELS 
labels = [1, 0, -1]
labels_names = ["CH", "PL", "PH"]
labels_table = {"1": "CH", "0": "PL", "-1": "PH"}
os.chdir("./RESULTS/ML")
for spl_times in [1,5,10]:
    for k_size in [5,6,7,8]:
        for w_tag_name in ["WEIGHTED", "unWEIGHTED"]:
            stats_file =f'seq_splx{spl_times}\\stats\\{k_size}_mer_RF_SPL_x{spl_times}_{w_tag_name}_stats_object.pkl'
            with open(stats_file, "rb") as f:
                stats = pickle.load(f)
            params = [f"splx{spl_times}"]
            outdir = f'seq_splx{spl_times}'
            outfile_base = os.path.basename(stats_file).split(".")[0]
            outfile_base = outfile_base.replace("_stats_object","") 
            lines = ["--","-","-.",":"]
            linecycler = cycle(lines)
            if w_tag_name == "unWEIGHTED":
                w_tag = " | unW"
            elif w_tag_name == "WEIGHTED":
                w_tag = " |  W "
            for model in models:
                for param in params: 
                    #print(f'{outfile_base}')     
                    exp1 = f'ROC_{param}_CV{total_fold}.svg'
                    exp2 = f'CM_{param}_CV{total_fold}.svg'
                    fig, ax = plt.subplots(figsize=(6,5))
                    for i in range(total_fold):
                        linestyle=next(linecycler)
                        my_plot_roc_curve(stats[f"{model}_{param}_test_y_test"][i], stats[f"{model}_{param}_test_y_score"][i],curves=('macro'), fold=i, linestyle=linestyle, ax=ax)
                    mean_ROCauc = np.mean(stats[f"{model}_{param}_test_roc_auc"]).round(3)
                    plt.title(f'Sampling x{spl_times}{w_tag} | k-mer = {k_size}\nCross-Validation AUC: {mean_ROCauc:.3f}',fontweight='bold')
                    plt.xlabel("False positive rate", labelpad=10)
                    plt.ylabel("True positive rate")
                    plt.savefig(f'{outdir}\\figures\\{outfile_base}_{exp1}', bbox_inches='tight')
                    
                    mtx = my_avg_confusion_matrix(stats[f'{model}_{param}_test_cm'])
                    plot_my_mini_confusion_matrix(mtx, labels=labels_names)
                    plt.title(f'Sampling x{spl_times}{w_tag} | k-mer = {k_size}\nCross-Validation performance', fontweight='bold')
                    plt.savefig(f'{outdir}\\figures\\{outfile_base}_{exp2}', bbox_inches='tight')
                    # plt.show()

                # AVERAGE HOLDOUT RESULTS 
                for model in models:
                    for param in params:
                        for test in tests:
                            stats[f'{model}_{param}_hold_{test}_avg'] = np.mean(stats[f'{model}_{param}_hold_{test}'], axis=0)
                    # PLOT: ROC AUC
                    exp3 = f'HOLD_ROC_{param}_{k_size}.svg'
                    exp4 = f'HOLD_CM_{param}_{k_size}.svg'  
                    mean_ROCauc_hold = np.mean(stats[f"{model}_{param}_hold_roc_auc_avg"]).round(3)
                    my_skplot_plot_roc_curve(stats[f"{model}_{param}_hold_y_test"][0], stats[f"{model}_{param}_hold_y_score_avg"],labels_table,curves=( 'each_class','macro'))
                    plt.title(f'Sampling x{spl_times}{w_tag} | k-mer = {k_size}\nHoldout AUC: {mean_ROCauc_hold:.3f}', fontweight='bold')
                    plt.savefig(f'{outdir}\\figures\\{outfile_base}_{exp3}', bbox_inches='tight')
                    # PLOT: CONFUSION MATRIX
                    mtx = my_avg_confusion_matrix(stats[f'{model}_{param}_hold_cm'])
                    plot_my_mini_confusion_matrix(mtx, labels=labels_names)
                    plt.title(f'Sampling x{spl_times}{w_tag} | k-mer = {k_size}\nHoldout performance', fontweight='bold')
                    plt.savefig(f'{outdir}\\figures\\{outfile_base}_{exp4}', bbox_inches='tight')