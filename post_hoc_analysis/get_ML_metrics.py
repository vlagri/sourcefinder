import os
import pandas as pd
import numpy as np
import pickle
from scipy import stats 
from sklearn.metrics import classification_report

###ask an user input for the base####

# Go to the main dir
BASE = os.makedirs("RESULTS", exist_ok=True)
BASE_DIR = f'{BASE}/ML'
BASE_OUT = f'{BASE}/ML_tables'
os.makedirs(BASE_OUT, exist_ok=True)
os.chdir(BASE_DIR)
# LIST all model types
model_dirs = os.listdir(f'{BASE_DIR}')
# LIST all params
model_params = [i.split("_")[1] for i in model_dirs]
# ZIP THEM
# [0] DIRECTORY , [1] PARAM for the DICT
models_zip = list(zip(model_dirs, model_params))

models = ["RF"]
columns=['model_type', 'k_size', 'weights', \
        'CV_mcc', 'CV_f1', 'CV_acc', 'CV_roc_auc', 'CV_roc_auc_sd', \
        'CV_Ch_precision', 'CV_Ch_recall', 'CV_Ch_f1', 'CV_Ch_support', \
        'CV_Pl_precision', 'CV_Pl_recall', 'CV_Pl_f1', 'CV_Pl_support', \
        'CV_Ph_precision', 'CV_Ph_recall', 'CV_Ph_f1', 'CV_Ph_support', \
        'CV_report_avg_acc', \
        'hold_mcc', 'hold_f1', 'hold_acc', 'hold_roc_auc', 'hold_roc_auc_sd', \
        'hold_Ch_precision', 'hold_Ch_recall', 'hold_Ch_f1', 'hold_Ch_support', \
        'hold_Pl_precision', 'hold_Pl_recall', 'hold_Pl_f1', 'hold_Pl_support', \
        'hold_Ph_precision', 'hold_Ph_recall', 'hold_Ph_f1', 'hold_Ph_support', \
        'hold_report_avg_acc', 'hold_report_avg_precision', 'hold_report_avg_recall', 'hold_report_avg_f1', \
        'hold_ambig_count', 'hold_count', 'CV_fit_time', 'CV_fit_time_std']
df = pd.DataFrame(columns=columns)
# For each model type:
for model_zip in models_zip:
    model_dir = model_zip[0]
    # LIST all statistics files
    statistics_file_list = os.listdir(f'{BASE_DIR}/{model_dir}/stats')
    # For each file, read it in
    for statistics_file in statistics_file_list:
        with open(f'{BASE_DIR}/{model_dir}/stats/{statistics_file}', "rb") as pkl_handle:
            statistics = pickle.load(pkl_handle)
        if "WEIGHTED" in statistics_file:
            weight_tag = statistics_file.split("_")[5]
            if weight_tag == "WEIGHTED":
                w_tag = "W"
            elif weight_tag == "unWEIGHTED":
                w_tag = 'unW'
        else:
            w_tag = ""
    # Iterate over the file
        for model in models:
            param = model_zip[1]
            modeltype = model_dir
            weights = w_tag
            k_size = statistics_file.split("_")[0]  # <<<<
            CV_mcc = np.mean(statistics[f'{model}_{param}_test_mcc'], axis=0)
            CV_f1 = np.mean(statistics[f'{model}_{param}_test_f1_score'], axis=0)
            CV_acc = np.mean(statistics[f'{model}_{param}_test_acc'], axis=0)
            CV_roc_auc = np.mean(
                statistics[f'{model}_{param}_test_roc_auc'], axis=0)
            CV_roc_auc_sd = np.std(statistics[f'{model}_{param}_test_roc_auc'], axis=0).round(3)
            hold_mcc = np.mean(statistics[f'{model}_{param}_hold_mcc'], axis=0)
            hold_f1 = np.mean(statistics[f'{model}_{param}_hold_f1_score'], axis=0)
            hold_acc = np.mean(statistics[f'{model}_{param}_hold_acc'], axis=0)
            hold_roc_auc = np.mean(
                statistics[f'{model}_{param}_hold_roc_auc'], axis=0)
            hold_roc_auc_sd = np.std(statistics[f'{model}_{param}_hold_roc_auc'], axis=0).round(3)
            d_acc = CV_acc - hold_acc
            d_roc_auc = CV_roc_auc - hold_roc_auc
            CV_fit_time = np.mean(statistics[f'{model}_{param}_fit_time_s'], axis=0)
            CV_fit_time_std = np.std(statistics[f'{model}_{param}_fit_time_s'], axis=0).round(3)
            # ADDING CLASS SPECIFIC STATS
            #############################
            CV_dump, CV_acc_dump = [], []
            hold_dump, hold_acc_dump, hold_precision_dump, hold_recall_dump, hold_f1_dump = [], [], [], [], []
            for fold in range(5):
                CV_y_true = statistics[f'{model}_{param}_test_y_test'][fold]
                CV_y_pred = statistics[f'{model}_{param}_test_y_pred'][fold]
                CV_report = classification_report(CV_y_true, CV_y_pred, labels = [1,0,-1], target_names = ["Ch", "Pl", "Ph"], output_dict = True, zero_division=0)
                CV_report_statistics = [[CV_report[i][j] for j in ['precision', 'recall', 'f1-score', 'support']] for i in ["Ch", "Pl", "Ph"]]
                CV_acc_dump.append(CV_report['accuracy'])
                CV_dump.append(CV_report_statistics)
            
                hold_y_true = statistics[f'{model}_{param}_hold_y_test'][fold]
                hold_y_pred = statistics[f'{model}_{param}_hold_y_pred'][fold]
                hold_report = classification_report(hold_y_true, hold_y_pred, labels = [1,0,-1], target_names = ["Ch", "Pl", "Ph"], output_dict = True, zero_division=0)
                hold_report_statistics = [[hold_report[i][j] for j in ['precision', 'recall', 'f1-score', 'support']] for i in ["Ch", "Pl", "Ph"]]
                hold_acc_dump.append(hold_report['accuracy'])
                hold_precision_dump.append(hold_report['macro avg']['precision'])
                hold_recall_dump.append(hold_report['macro avg']['recall'])
                hold_f1_dump.append(hold_report['macro avg']['f1-score'])
                hold_dump.append(hold_report_statistics)
            # >>>>>>>>>>>>>>>>>>>>>>>>
            CV_report_means = np.mean(CV_dump, axis=0)
            CV_Ch_precision, CV_Ch_recall, CV_Ch_f1, CV_Ch_support = CV_report_means[0][0], CV_report_means[0][1], CV_report_means[0][2], CV_report_means[0][3]
            CV_Pl_precision, CV_Pl_recall, CV_Pl_f1, CV_Pl_support = CV_report_means[1][0], CV_report_means[1][1], CV_report_means[1][2], CV_report_means[1][3]
            CV_Ph_precision, CV_Ph_recall, CV_Ph_f1, CV_Ph_support = CV_report_means[2][0], CV_report_means[2][1], CV_report_means[2][2], CV_report_means[2][3]
            CV_report_acc = np.mean(CV_acc_dump, axis=0)
            # <<<<<<<<<<<<<<<<<<<<<<<<
            ############################
            # >>>>>>>>>>>>>>>>>>>>>
            hold_report_means = np.mean(hold_dump, axis=0)
            hold_ambig = [idx for idx,val in enumerate(stats.mode(statistics[f'{model}_{param}_hold_y_pred'])[1][0]) if val <3] # from 5 models, where is no majority
            hold_ambig_count = len(hold_ambig)
            hold_count = len(statistics[f'{model}_{param}_hold_y_pred'][0])
            hold_Ch_precision, hold_Ch_recall, hold_Ch_f1, hold_Ch_support = hold_report_means[0][0], hold_report_means[0][1], hold_report_means[0][2], hold_report_means[0][3]
            hold_Pl_precision, hold_Pl_recall, hold_Pl_f1, hold_Pl_support = hold_report_means[1][0], hold_report_means[1][1], hold_report_means[1][2], hold_report_means[1][3]
            hold_Ph_precision, hold_Ph_recall, hold_Ph_f1, hold_Ph_support = hold_report_means[2][0], hold_report_means[2][1], hold_report_means[2][2], hold_report_means[2][3]
            hold_report_acc = np.mean(CV_acc_dump, axis=0)
            hold_report_precision =  np.mean(hold_precision_dump, axis=0)
            hold_report_recall =  np.mean(hold_recall_dump, axis=0)
            hold_report_f1 =  np.mean(hold_f1_dump, axis=0)
            # <<<<<<<<<<<<<<<<<<<<<<<<
            #############################

            avg_statistics = pd.Series([modeltype, k_size, weights, \
                       CV_mcc, CV_f1, CV_acc, CV_roc_auc, CV_roc_auc_sd, \
                       CV_Ch_precision, CV_Ch_recall, CV_Ch_f1, CV_Ch_support, \
                       CV_Pl_precision, CV_Pl_recall, CV_Pl_f1, CV_Pl_support, \
                       CV_Ph_precision, CV_Ph_recall, CV_Ph_f1, CV_Ph_support, \
                       CV_report_acc, \
                       hold_mcc, hold_f1, hold_acc, hold_roc_auc, hold_roc_auc_sd, \
                       hold_Ch_precision, hold_Ch_recall, hold_Ch_f1, hold_Ch_support, \
                       hold_Pl_precision, hold_Pl_recall, hold_Pl_f1, hold_Pl_support, \
                       hold_Ph_precision, hold_Ph_recall, hold_Ph_f1, hold_Ph_support, \
                       hold_report_acc, hold_report_precision, hold_report_recall, hold_report_f1, \
                       hold_ambig_count, hold_count, CV_fit_time, CV_fit_time_std], index=df.columns)
            df = df.append(avg_statistics, ignore_index=True)

df = df.sort_values(['model_type', 'weights', 'k_size'], ascending=[0, 1, 1])
df = df.round(3)
# Open a file for output
filename = f'{BASE_OUT}/statistics_average_complete_sorted2.tsv'
df.to_csv(filename, sep="\t")

df1 = df.sort_values(['hold_acc'], ascending=[0])
filename2 = f'{BASE_OUT}/statistics_average_acc_complete2.tsv'
df1.to_csv(filename2, sep="\t")

print(f'Results: {BASE_OUT}')
